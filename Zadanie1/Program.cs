﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using GLFW;
using GlmSharp;

using Shaders;
using Models;
using OpenTK.Mathematics;
using Microsoft.VisualBasic.FileIO;

namespace PMLabs
{
    public class BC : IBindingsContext
    {
        public IntPtr GetProcAddress(string procName)
        {
            return Glfw.GetProcAddress(procName);
        }
    }

    class Program
    {
        static Sphere sphere = new Sphere(1, 24, 24);
        static Torus A = new Torus(1.3f, 0.05f, 100, 20);
        static Torus B = new Torus(1.4f, 0.05f, 100, 20);
        static Torus C = new Torus(1.6f, 0.05f, 100, 20);
        static Torus D = new Torus(1.7f, 0.05f, 100, 20);
        static Torus F = new Torus(1.9f, 0.05f, 100, 20);

        static float planetSpeed = 2 * (float)Math.PI / 10; // One rotation in 10 seconds
        static float ringSpeedFactor = 0.5f; // Adjust the ring rotation speed factor as needed

        static float planetAngleX = 90;
        static float planetAngleY = 0;

        static float animationSpeed = 2*(float)(Math.PI / 10);

        static float speed_y = animationSpeed;
        static float speed_x; 


        static vec3 cameraPosition = new vec3(0.0f, 0.0f, -5f);

        public static void InitOpenGLProgram(Window window)
        {
            GL.ClearColor(0, 0, 0, 1);
            DemoShaders.InitShaders("Shaders\\");
            GL.Enable(EnableCap.DepthTest);
        }

        public static void DrawScene(Window window, float angle_x, float angle_y)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            mat4 V = mat4.LookAt(
                cameraPosition,
                new vec3(0.0f, 0.0f, 0.0f),
                new vec3(0.0f, 1.0f, 0.0f));
            mat4 P = mat4.Perspective(glm.Radians(50.0f), 1.0f, 1.0f, 50.0f);

            DemoShaders.spLambert.Use();
            GL.Uniform4(DemoShaders.spLambert.U("color"), 255f / 255f, 204f / 255f, 102f / 255f, 1f);

            GL.UniformMatrix4(DemoShaders.spLambert.U("P"), 1, false, P.Values1D);
            GL.UniformMatrix4(DemoShaders.spLambert.U("V"), 1, false, V.Values1D);

            mat4 M = mat4.Rotate(planetAngleX, new vec3(0, 1, 0)) * mat4.Rotate(planetAngleY, new vec3(1, 0, 0));
            GL.UniformMatrix4(DemoShaders.spLambert.U("M"), 1, false, M.Values1D);

            sphere.drawSolid();

            // Update and draw the rings
            DrawRing(A);
            DrawRing(B);
            DrawRing(C);
            DrawRing(D);
            DrawRing(F);

            Glfw.SwapBuffers(window);
        }

        static void DrawRing(Torus ring)
        {
            mat4 M = mat4.Rotate(planetAngleX, new vec3(0, 1, 0)) * mat4.Rotate(planetAngleY, new vec3(1, 0, 0));
            GL.UniformMatrix4(DemoShaders.spLambert.U("M"), 1, false, M.Values1D);

            ring.drawSolid();
        }


        public static void FreeOpenGLProgram(Window window)
        {
            // Cleanup resources if needed
        }

        static void Main(string[] args)
        {
            Glfw.Init();

            Window window = Glfw.CreateWindow(500, 500, "Programowanie multimedialne", GLFW.Monitor.None, Window.None);

            Glfw.SetKeyCallback(window, KeyProcessor);
            Glfw.MakeContextCurrent(window);
            Glfw.SwapInterval(1);

            GL.LoadBindings(new BC());

            InitOpenGLProgram(window);

            Glfw.Time = 0;

            while (!Glfw.WindowShouldClose(window))
            {
                // Update planet and ring angles
                UpdateAngles();

                // Draw the scene
                DrawScene(window, 0, 0);

                Glfw.PollEvents();
            }

            FreeOpenGLProgram(window);

            Glfw.Terminate();
        }

        static void UpdateAngles()
        {
            planetAngleX += speed_y * (float)Glfw.Time;
            planetAngleY += speed_x * (float)Glfw.Time;
            Glfw.Time = 0;
        }

        public static void KeyProcessor(System.IntPtr window, Keys key, int scanCode, InputState state, ModifierKeys mods)
        {
            float scaleStep = 0.3f;
            
            if (state == InputState.Press)
            {
                switch (key)
                {
                    case Keys.Left:
                        speed_y = -animationSpeed;
                        speed_x = 0;
                        break;
                    case Keys.Right:
                        speed_y = animationSpeed;
                        speed_x = 0;
                        break;
                    case Keys.Up:
                        speed_x = -animationSpeed;
                        speed_y = 0;
                        break;
                    case Keys.Down:
                        speed_x = animationSpeed;
                        speed_y = 0;
                        break;
                    case Keys.PageUp:
                        // Move the camera closer
                        cameraPosition += new vec3(0.0f, 0.0f, scaleStep);
                        break;
                    case Keys.PageDown:
                        // Move the camera farther
                        cameraPosition -= new vec3(0.0f, 0.0f, scaleStep);
                        break;
                    case Keys.Numpad0:
                        animationSpeed = 0f;
                        break;
                    case Keys.Numpad1:
                        animationSpeed = (float)(Math.PI / 8);
                        break;
                    case Keys.Numpad2:
                        animationSpeed = (float)(Math.PI / 7);
                        break;
                    case Keys.Numpad3:
                        animationSpeed = (float)(Math.PI / 6);
                        break;
                    case Keys.Numpad4:
                        animationSpeed = 2*(float)(Math.PI / 10);
                        break;
                    case Keys.Numpad5:
                        animationSpeed = (float)(Math.PI / 4.5f);
                        break;
                    case Keys.Numpad6:
                        animationSpeed = (float)(Math.PI / 4);
                        break;
                    case Keys.Numpad7:
                        animationSpeed = (float)(Math.PI / 3);
                        break;
                    case Keys.Numpad8:
                        animationSpeed = (float)(Math.PI / 2);
                        break;
                    case Keys.Numpad9:
                        animationSpeed = (float)Math.PI;
                        break;
                }
            }
            else if (state == InputState.Release)
            {
                speed_x = 0;
                speed_y = animationSpeed; 
            }
        }
    }
}
